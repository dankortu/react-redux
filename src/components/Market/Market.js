import React, { Component } from 'react';
import Order from '../Order/Order'
import './Market.css';
import {createOrder, moveOrderToFarm} from 'actions/marketActions';
import {connect} from 'react-redux';

let id = 0;

const getId = () => {
  id += 1;
  return id;
};

export const vegetables = [
  'Капуста',
  'Редиска',
  'Огурцы',
  'Морковь',
  'Горох',
  'Баклажан',
  'Тыква',
  'Чеснок',
  'Лук',
  'Перец',
  'Картофель',
  'Редька',
];

const getNewOrder = () => {
  return {
    id: getId(),
    name: vegetables[Math.floor(Math.random() * vegetables.length)],
    price: 100 + Math.floor(Math.random() * 500),
    createdAt: new Date().toTimeString(),
  };
};




export class Market extends Component {

  onClickCreateOrder = () => {
    const { createOrder } = this.props;
    
    createOrder(getNewOrder());
  }

  onClickMoveOrderToFarm = () => {
    const { moveOrderToFarm, orders } = this.props;

    moveOrderToFarm(orders[0]);
  }


  render() {
    const {
      onClickMoveOrderToFarm ,
      onClickCreateOrder,
      props: { orders }
    } = this;

    return (
      <div className="market component-container">
        <div className="new-orders__create-form ">
          <button className="create-order-btn btn" onClick={onClickCreateOrder}>
            Создать заказ
          </button>
          <button className="move-order-to-farm-btn btn" onClick={onClickMoveOrderToFarm} disabled={orders.length===0}>
            Отправить заказ на ферму
          </button>
        </div>

        <div className = "order-list">
          {
            orders.map(
              (order) => <Order name={order.name} price={order.price} createdAt={order.createdAt} key={order.id}/>
            )
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({ orders: state.market.orders })

const mapDispatchToProps = { createOrder, moveOrderToFarm }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)( Market );
