import React, { Component } from 'react';
import Order from '../Order/Order'
import {moveOrderToCustomer} from 'actions/farmActions';
import {connect} from 'react-redux';

export class Farm extends Component {

  onClickMoveOrderToCustomer = () => {
    const { orders, moveOrderToCustomer } = this.props;

    moveOrderToCustomer(orders[0]);
  }

  render() {
    const { 
      onClickMoveOrderToCustomer,
      props: { orders }
    } = this;

    return (
      <div className="farm component-container">
        <button className="move-order-to-customer-btn btn" onClick={onClickMoveOrderToCustomer} disabled={orders.length === 0}>
          Отправить заказ клиенту
        </button>
        {
          orders.map(
            (order) => <Order name={order.name} price={order.price} createdAt={order.createdAt} key={order.id}/>
          )
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({ orders: state.farm.orders })

const mapDispatchToProps = { moveOrderToCustomer }

export default connect(
  mapStateToProps,
  mapDispatchToProps
)( Farm );

