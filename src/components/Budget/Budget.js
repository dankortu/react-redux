import React from 'react';
import {connect} from 'react-redux';

export const Budget = (props) => {
  const {deliveryExpanse, profit, farmExpanse} = props;

  return (
    <div className="budget component-container">
      <p>Выручка с доставки: {deliveryExpanse}₸</p>
      <p>Выручка со стоимости заказа: {profit}₸</p>
      <p>Выручка с фермы: {farmExpanse}₸</p>
      <p>Итого: {farmExpanse + profit + deliveryExpanse}₸</p>
    </div>
  )
};



const mapStateToProps = (state) => (
  {
    deliveryExpanse: state.budget.deliveryExpanse, 
    profit: state.budget.profit, 
    farmExpanse: state.budget.farmExpanse 
  }
)

export default connect( mapStateToProps )( Budget );

